CKEditor Configuration Helper

Introduction
============
This module allows adding some advanced settings to CKEditor. In example if you need a placeholder in wysiwyg this
module is for you ;)
Check https://ckeditor.com/cke4/addon/confighelper for details.

Installation
============

This module requires the core CKEditor module and also the Configuration Helper plugin from CKEditor.com.

1. Download the plugin from https://ckeditor.com/cke4/addon/confighelper at least version: 1.8.3.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable CKEditor Configuration Helper module in the Drupal admin.
4. Go to text format configuration, check "CKEditor Configuration Helper plugin" tab for settings.

Follow these steps to make sure the plugin works for Basic HTML text format:

1. Add placeholder value to textarea field which using CKEditor or you can add default placeholder to all wysiwyg
   entries in "CKEditor Configuration Helper plugin" tab text format settings.
2. Make sure placeholder appears.
3. Check other settings on "CKEditor Configuration Helper plugin".
